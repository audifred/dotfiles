# Luis' Dotfiles
1. Clone repo into the root directory.
2. Install brew https://brew.sh/
    - Run `brew.sh` to install formulae
3. Run ./.macos to update OSX default settings.
4. Create the following symlinks
    - `ln -s ~/.dotfiles/.zshrc ~/.zshrc`
    - `ln -s ~/.dotfiles/VSCode/settings.json ~/Library/Application\ Support/Code/User/settings.json`
5. Download the following VSCode extensions
    - Auto rename tags
    - ES7 snippets
    - ES Lint
    - Prettier
    - Rainbow brackets
    - VSCode styled components
