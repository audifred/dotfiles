!#bin/zsh


# Insert brew installation command here
xcode-select --install

brew cask install firefox
brew cask install brave
brew cask install alfred
brew cask install google-chrome
brew cask install 1password
brew cask install visual-studio-code
 
 #for Emacs
 brew install git ripgrep
 brew install coreutils fd
 brew tap d12frosted/emacs-plus
 brew install emacs-plus@27 --with-modern-icon-cg433n
ln -s /usr/local/opt/emacs-plus/Emacs.app /Applications/Emacs.app

git clone https://github.com/hlissner/doom-emacs ~/.emacs.d
~/.emacs.d/bin/doom install