autoload -Uz promptinit && promptinit
autoload -Uz compinit && compinit

prompt pure

autoload -Uz colors && colors

setopt auto_cd
setopt correct

zstyle :prompt:pure:path color cyan
zstyle :prompt:pure:git:branch color yellow
# precmd() {
#   gitStatus="$(git symbolic-ref --short -q HEAD 2> /dev/null)"
#   NEWLINE=$'\n'
#
#   PROMPT="%~ %{$fg[yellow]%}${gitStatus}${NEWLINE}%{$fg[magenta]%}❯ %{$reset_color%}"
# }


if [ -f ~/.dotfiles/.aliases ]; then
    source ~/.dotfiles/.aliases
fi

if [ -f ~/.dotfiles/.functions ]; then
    source ~/.dotfiles/.functions
fi


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export PATH="/Applications/MacPorts/Emacs.app/Contents/MacOS:$PATH"
